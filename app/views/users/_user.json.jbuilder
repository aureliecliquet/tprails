json.extract! user, :id, :name, :firstname, :mail, :created_at, :updated_at
json.url user_url(user, format: :json)